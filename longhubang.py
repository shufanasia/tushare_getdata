# -*- coding: utf-8 -*-  
from extern import *
import json
import time
if  __name__=='__main__':
    start_day='20200101'
    pro = getpro()
    con,db=mongocon()
    collist=db.list_collection_names()
    is_exists=False
    if 'top_inst' in collist:
        print('table is exists.')
        is_exists=True
    mftb = db['top_inst']
    if is_exists==False:
        tradedays=trade_days(start_date=start_day, end_date=latestday())
    else:
        lastday=list(mgtodf(mftb.find({'trade_date':{'$gte':start_day}}))['trade_date'].sort_values())[-1]
        tradedays=trade_days(start_date=lastday, end_date=latestday())
    times=0
    for i in tradedays[1:]:
        print(i,'top_inst')
        try:
            df2=pro.top_inst(trade_date=i)
        except:
            print('error, waiting 60s')
            time.sleep(60)
            df2=pro.top_inst(trade_date=i)
        times+=1
        if times>=190:
            print('mid waiting...')
            time.sleep(60)
            times=0
        if df2.size>0:
            rec = json.loads(df2.T.to_json()).values()
            nums = mftb.insert_many(rec)
        else:print(i,'data not update')
