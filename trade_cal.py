# -*- coding: utf-8 -*-  
from extern import *
import json
if  __name__=='__main__':
    start_day='20000101'
    end_date='20201231'
    pro = getpro()
    con,db=mongocon()
    collist=db.list_collection_names()
    is_exists=False
    if 'trade_cal' in collist:
        print('table is exists.')
        is_exists=True
    mftb = db['trade_cal']
    if is_exists==False:
        df = pro.trade_cal(exchange='', start_date=start_day, end_date=end_date,is_open=1)
        tradedays=list(df['cal_date'])
    else:
        lastday=list(mgtodf(mftb.find({}))['cal_date'])[-1]
        df = pro.trade_cal(exchange='', start_date=lastday, end_date=end_date,is_open=1)
        tradedays=list(df['cal_date'])[1:]
        print('....')
    if len(tradedays)<=0:
        print('data not update intime')
    else:
        #df=df.drop(0)
        #print(df)
        if len(tradedays)==len(df.index):
            rec = json.loads(df.T.to_json()).values()
            nums = mftb.insert_many(rec)
            print('data update over')
